cmakeの使い方
==========

この文章は[GitLab][GitLab]上で開発されており継続的に更新されていきます。

[GitLab]: https://gitlab.com/termoshtt/cmake-book

Contribution
------------
この本は下記のようにGNU フリー文書利用許諾契約書の元で配布されています。
タイポの指摘や内容に関する不明瞭な点の指摘などがあれば[GitLab/issue][issue]に報告してください。

[issue]: https://gitlab.com/termoshtt/cmake-book/issues

License
--------
Copyright (C) 2020 Toshiki Teramura

この文書を、フリーソフトウェア財団発行の [GNU フリー文書利用許諾契約書](https://gitlab.com/termoshtt/cmake-book/blob/master/LICENSE)(バージョン1.2かそれ以降から一つを選択)が定める条件の下で複製、頒布、あるいは改変することを許可する。変更不可部分、表カバーテキスト、裏カバーテキストは存在しない。
